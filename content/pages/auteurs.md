# Étapes de l'écriture et de l'édition de vos articles sur *Stylo*

<p>La revue <em>Sens public</em> s&rsquo;engage dans la production de contenus scientifiques structur&eacute;s et conformes aux formats standards libres. Nous sommes en effet convaincu&middot;e&middot;s que la connaissance ne doit pas &ecirc;tre enferm&eacute;e dans des formats propri&eacute;taires comme le format .docx.</p>

<p><em>Sens public</em> a donc fait des choix techniques en ce sens, en cr&eacute;ant l&rsquo;&eacute;diteur de texte <a href="https://stylo.ecrituresnumeriques.ca/">Stylo</a>, qui produit du HTML &agrave; partir des formats <a href="https://via.hypothes.is/https://daringfireball.net/projects/markdown/">markdown</a>, yaml et bibtex, <a href="https://via.hypothes.is/https://memoire.quaternum.net/">d&eacute;sormais devenus des standards pour l&rsquo;&eacute;dition savante</a>. Stylo propose un environnement d&rsquo;&eacute;dition tr&egrave;s simple, qui ne requiert aucune comp&eacute;tence informatique particuli&egrave;re. 

La chaîne éditoriale de la revue Sens public est articulée autour de cet outil ; il importe donc que vous le preniez en main pour réaliser votre article.

Stylo est conçu pour accueillir toutes les étapes de l'élaboration d'un article, dès le début de son écriture. Cependant, il est possible que vous disposiez déjà d'une version de votre texte en Word ; sur cette page, nous vous guiderons à travers ces deux cas de figure.



## 1. Initialiser votre article

Tout d'abord, comme nous avons déjà le texte en format Word, il faut le convertir au format markdown.

*Attention : un copier-coller simple depuis word ne convient pas et vous fera perdre du temps ainsi que le travail que vous avez déjà effectué en amont pour structurer votre texte.*

Stylo met à votre disposition un convertisseur : https://stylo-export.ecrituresnumeriques.ca/

Il suffit de coller le lien de partage dropbox dans le convertisseur. Ensuite, vous téléchargez le zip, et ouvrez le fichier se terminant par l'extension .md dans un éditeur de texte de base (par exemple, textEdit sur mac ou Notepad sur pc). Vous devriez avoir devant les yeux une version en md de votre article, que vous pouvez maintenant copier-coller dans un nouvel article Stylo.

## 2. Travailler votre article
Tout s'enregistre automatiquement sur Stylo. Vous pouvez enregistrer des versions précises de votre article, mais tout est enregistré au fur et à mesure.
On peut scinder le travail sur l'article en trois étapes principales lorsque l'on part d'un document Word : 

### Renseigner les métadonnées
Cette étape n'est pas très longue ; cliquez sur le petit onglet *metadata* en haut à droite. Inscrivez, en *basic mode*, le titre de votre article. Ajoutez-vous comme auteur. Vous devez fournir un identifiant unique Orcid : créez-le ici : https://orcid.org/
Si vous avez déposé un mémoire sur papyrus, vous l'avez peut-être déjà créé : sinon, vous prenez un peu d'avance!

Vous devez aussi ajouter votre résumé et vos mots-clefs. Vous pourrez le modifier plus tard si nécessaire. Les autres champs peuvent être laissés vides pour l'instant.

### « Nettoyer » l'article

En effectuant la conversion depuis Word, vous avez pu garder la plupart de la structure de votre article (les citations, les intertitres, les notes, etc). En général, vous n'aurez qu'à vérifier que des bouts de codes résiduels ne se soient pas glissés dans l'article, que vos citations soient bien indiquées et que vous avez bien mis les espaces insécables partout où cela était nécessaire.

*Les espaces insécables apparaissent comme de petits points médians sur Stylo. Parfois, certains navigateurs ne le affichent pas, même s'ils sont bien là. Si vous n'en voyez pas du tout, essayez de changer de navigateur (normalement cela fonctionne à tout coup sur Firefox), sinon, partagez-moi votre article et envoyez moi un petit message, on regardera ça.*

### Gérer les références bibliographiques
Cette partie est normalement un peu plus longue. 

L'essentiel des informations nécessaires se trouvent ici : http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/bibliographie.md

Il vous faut intégrer les références bibliographiques de votre article à même votre texte. Pour ce faire, vous devez posséder un fichier bibtex de votre bibliographie, qu'il vous sera facile d'obtenir si vous utilisez zotero, Mendeley ou EndNote, par exemple. Sinon, vous devrez vous créer un compte zotero et y entrer vos références (https://bib.umontreal.ca/citer/logiciels-bibliographiques/zotero/installer). Les bibliothèques de l'université disposent d'une documentation très complète à ce sujet.
Vous pouvez ensuite exporter votre fichier bibtex, et copier-coller vos références dans l'onglet Bibliography : manage bibliography.

Si vous utilisez zotero, vous pouvez aussi directement synchroniser votre bibliothèque avec votre article.

Vous devrez remplacer les appels de note ne servant qu'à indiquer une référence par la « clef bibtex » du document cité. 

Au fur et à mesure, vous devrez donc effacer les notes en bas de page au bas de l'article. Il ne restera plus que les notes servant de précisions, de traduction, etc. Pas besoin de les renuméroter, les prévisualisations et les exports les remettront dans le bon ordre.

### Derniers points
La bibliographie apparaîtra automatiquement en fin d'article. Vous devez toutefois ajouter son titre : ## Bibliographie

Vous pouvez en tout temps prévisualiser votre article en cliquant sur le bouton « preview » de votre version en cours d'édition. Lorsque vous arrivez à un résultat satisfaisant, vous pouvez partager votre article.

## 3. Partager votre article

Tel que détaillé dans le protocole du publication (http://www.sens-public.org/publier/), le temps est maintenant venu de partager votre article! Vous le partagerez d'abord auprès de nous, mais ensuite auprès de la revue, bien évidemment.

L'adresse de la revue : proposition@sens-public.org

En ce qui concerne la soumission de votre article, il vous faut partager l'article dans Stylo, mais aussi envoyer un  courriel en plus à l'adresse de la revue, dans lequel vous les avisez du partage de votre texte et éventuellement du dossier dans lequel vous vous inscrivez. Ainsi, Sens public disposera de votre adresse de correspondance et nous pourrons facilement échanger avec vous pour la suite des choses.

Vous sera demandée une image **libre de droits** pour illustrer votre article.

## 4. Les annotations 
La codirection du dossier comme la revue vous fera parvenir des retours sur vos textes. Ceux-ci prennent la forme d'annotations effectuées avec l'outil hypothes.is sur une prévisualisation de votre article. Vous devrez vous créer un compte personnel, mais le fonctionnement est très simple.


## Liens utiles
- Lien vers Stylo : https://stylo.huma-num.fr/

- Marche à suivre sur Stylo : https://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/premierspas.md
N'hésitez pas à explorer ce site, qui contient toute la documentation nécessaire sur l'utilisation de Stylo.

- Les bases du MarkDown : https://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/syntaxemarkdown.md

- Protocole de soumission des articles : http://www.sens-public.org/publier/

- Lien vers la permanence (les jeudis de 11h à 12h) : https://meet.jit.si/stylo

- Guide Zotero des bibliothèques de l'UdeM : https://bib.umontreal.ca/citer/logiciels-bibliographiques/zotero/installer
- Lien vers Zotero : https://www.zotero.org/
- Explications sur l'annotation avec hypothes.is : http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/preview.md




<h4>Comment soumettre des articles &agrave; Sens public ?</h4>

<p>Les articles pour Sens public doivent &ecirc;tre r&eacute;dig&eacute;s avec l&rsquo;&eacute;diteur de texte <a href="https://via.hypothes.is/https://stylo.ecrituresnumeriques.ca/">Stylo</a> puis partag&eacute;s avec l&rsquo;adresse de la r&eacute;daction (proposition@sens-public.org) directement depuis la plateforme Stylo. Un courriel doit ensuite &ecirc;tre envoy&eacute; &agrave; l&#39;adresse proposition@sens-public.org pour signaler la soumission.</p>

<p><a href="https://via.hypothes.is/http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md"><em>La documentation compl&egrave;te sur l&rsquo;usage de Stylo est disponible ici</em></a></p>

<h4>Comment cr&eacute;er un article sur Stylo ?</h4>

<p>Vous devez d&rsquo;abord vous <a href="https://front.stylo.ecrituresnumeriques.ca/register">cr&eacute;er un compte</a> sur la plateforme.</p>

<p>Vous aurez ensuite acc&egrave;s &agrave; l&rsquo;interface, qui se pr&eacute;sente comme suit :</p>

<p><img alt="Stylo - page d’accueil" src="https://i.imgur.com/IIQBlkJ.png" /></p>

<p>Stylo - page d&rsquo;accueil</p>

<p>Par d&eacute;faut, un premier article &ldquo;How to Stylo&rdquo; est pr&eacute;sent sur la plateforme, et d&eacute;taille chaque &eacute;tape de la r&eacute;daction d&rsquo;un article. N&rsquo;h&eacute;sitez pas &agrave; vous y r&eacute;f&eacute;rer si vous souhaitez directement &eacute;crire votre article dans Stylo, ou si vous avez des questions sur des articles &agrave; traitement particulier (illustrations, etc.).</p>

<p>Cliquez sur le bouton &ldquo;Create a new article&rdquo; (vous devrez renseigner le titre de l&rsquo;article dans la case pr&eacute;vue &agrave; cet effet, puis cliquer &agrave; nouveau sur le bouton &ldquo;create&rdquo;).</p>

<p><img alt="Edit" src="https://i.imgur.com/Tr0uctT.png" /></p>

<p>Edit</p>

<p>L&rsquo;article apparait d&eacute;sormais dans votre liste d&rsquo;articles. Cliquez sur &ldquo;Edit&rdquo; pour acc&eacute;der &agrave; l&rsquo;environnement d&rsquo;&eacute;dition, qui se pr&eacute;sente ainsi :</p>

<p><img alt="Environnement d'édition stylo" src="https://i.imgur.com/nBPfMBs.png" /></p>

<p>L&rsquo;environnement d&rsquo;&eacute;dition est compos&eacute; de 5 modules :</p>

<ul>
	<li>au centre : l&rsquo;espace d&rsquo;&eacute;criture, consacr&eacute; au corps de texte de l&rsquo;article</li>
	<li>&agrave; droite : le bouton [Metadata] ouvre l&rsquo;&eacute;diteur de m&eacute;tadonn&eacute;es</li>
	<li>&agrave; gauche :
	<ul>
		<li>les Versions pour naviguer et agir sur les diff&eacute;rentes versions de l&rsquo;article</li>
		<li>le Sommaire de l&rsquo;article liste les titres de niveau 2 et 3</li>
		<li>la Bibliographie liste les r&eacute;f&eacute;rences bibliographiques</li>
		<li>les Statistiques offrent quelques donn&eacute;es quantitatives sur l&rsquo;article</li>
	</ul>
	</li>
</ul>

<h4>Comment r&eacute;diger votre article ?</h4>

<p>Pour d&eacute;poser votre article, il vous suffit de suivre les quatre &eacute;tapes suivantes :</p>

<h5>1. Renseigner les m&eacute;tadonn&eacute;es de l&rsquo;article</h5>

<p>Ouvrez l&rsquo;&eacute;diteur de m&eacute;tadonn&eacute;es en cliquant sur le bouton &ldquo;Metadata&rdquo; qui se trouve sur votre droite.</p>

<p>Les m&eacute;tadonn&eacute;es doivent obligatoirement comprendre les &eacute;l&eacute;ments suivants :</p>

<ul>
	<li>Titre (et sous-tire, le cas &eacute;ch&eacute;ant)</li>
	<li>Un r&eacute;sum&eacute; en <em>deux langues</em></li>
	<li>Des mots-cl&eacute;s en <em>deux langues</em></li>
	<li>Le nom de&middot;s auteur&middot;e&middot;s ainsi que leur identifiant ORCID (voir <a href="https://via.hypothes.is/https://orcid.org/">ici</a> pour cr&eacute;er son identifiant ORCID)</li>
</ul>

<h5>2. R&eacute;diger le corps de l&rsquo;article</h5>

<p>L&rsquo;article doit &ecirc;tre r&eacute;dig&eacute; en markdown (un langage de balisage simplifi&eacute;, tr&egrave;s facile &agrave; assimiler et &agrave; utiliser). Pour apprendre en 3 minutes comment &eacute;crire en Markdown, vous pouvez consulter <a href="https://via.hypothes.is/http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/syntaxemarkdown.md">cette page</a>.</p>

<p>Si vous avez travaill&eacute; avec un logiciel de type word, Stylo s&rsquo;est d&ocirc;t&eacute; d&rsquo;un convertisseur markdown que vous trouverez <a href="https://via.hypothes.is/https://stylo-export.ecrituresnumeriques.ca/importdocx.html">ici</a>. Une fois votre article converti en format markdown, il vous suffira de le coller dans la partie centrale de l&rsquo;&eacute;diteur.</p>

<h5>3. Structurer les r&eacute;f&eacute;rences</h5>

<p>Tous les articles doivent &ecirc;tre accompagn&eacute;s d&rsquo;une bibliographie structur&eacute;e, en format bibtex. Vous pouvez directement structurer vos r&eacute;f&eacute;rences en bibtex, ou exporter vos r&eacute;f&eacute;rences en bibtex gr&acirc;ce &agrave; votre outils de gestion de bibliographie, tels que Zotero ou Mendeley (voir le tutoriel de Zotero <a href="https://via.hypothes.is/http://sens-public.org/IMG/pdf/Utiliser_Zotero.pdf">ici</a> et celui de Mendeley <a href="https://via.hypothes.is/https://libguides.usask.ca/c.php?g=218034&amp;p=1446316">l&agrave;</a>).</p>

<p>Pour ajouter une bibliographie, il vous faut cliquer sur le bouton &ldquo;manage bibliography&rdquo; qui se trouve dans le volet &agrave; gauche.</p>

<p>Une nouvelle fen&ecirc;tre s&rsquo;ouvrira alors, dans laquelle vous pourrez soit :</p>

<ul>
	<li>copier-coller l&rsquo;url de votre biblioth&egrave;que Zotero</li>
	<li>copier-coller le fichier bibtex &ldquo;brut&rdquo; dans la case pr&eacute;vue &agrave; cet effet</li>
</ul>

<p>Si, en cours d&rsquo;&eacute;criture, vous souhaitez ajouter de nouvelles r&eacute;f&eacute;rences, vous pouvez simplement les int&eacute;grer via la case &ldquo;citation&rdquo;</p>

<p>Les r&eacute;f&eacute;rences doivent &ecirc;tre reli&eacute;es dans le corps du texte comme <a href="https://via.hypothes.is/http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/bibliographie.md">expliqu&eacute; ici</a>. Pour ajouter une r&eacute;f&eacute;rence &agrave; l&rsquo;article, il suffit de cliquer sur la r&eacute;f&eacute;rence, puis de coller la r&eacute;f&eacute;rence dans le texte &agrave; l&rsquo;endroit souhait&eacute;. Ainsi, un clic revient &agrave; &ldquo;copier&rdquo; la cl&eacute; de la r&eacute;f&eacute;rence dans le presse-papier. Il ne reste plus qu&rsquo;&agrave; la coller dans le corps de texte.</p>

<p><img alt="Volet bibliographie" src="https://i.imgur.com/mxgawmi.png" /></p>

<p>&nbsp;</p>

<h5>4. Enregistrer l&rsquo;article</h5>

<p>Stylo sauvegarde automatiquement votre travail.</p>

<p>Cependant, il vous est possible - et fortement conseill&eacute; - d&rsquo;utiliser les fonctions de sauvegarde du logiciel, qui vous permet de nommer les diff&eacute;rentes versions de votre article.</p>

<p><img alt="" src="https://i.imgur.com/uen9d35.png" /></p>

<p>&nbsp;</p>

<p>Ainsi, lorsque vous &ecirc;tes parvenu&middot;e&middot;s &agrave; une version que vous jugez satisfaisante, vous pouvez par exemple inscrire, dans la case &ldquo;Label of the version&rdquo;, la mention &ldquo;soumission Sens public&rdquo;, puis cliquer sur le bouton &ldquo;Save major&rdquo;.</p>

<p>&nbsp;</p>

<p><strong>&Agrave; tout moment, vous pouvez visualiser votre travail en cliquant sur le bouton preview.</strong></p>

<p>La pr&eacute;visualisation est accompagn&eacute;e d&rsquo;un logiciel d&rsquo;annotation, qui pourra servir &agrave; l&rsquo;&eacute;valuation de votre article.</p>

<h4>Comment soumettre votre article ?</h4>

<p>Lorsque vous &ecirc;tes parvenu&middot;e&middot;s &agrave; une version finale de votre article, il vous suffit simplement le partager avec la r&eacute;daction. Vous devez :</p>

<ul>
	<li>Retourner sur la page Article de Stylo (en cliquant sur le bouton &ldquo;My articles&rdquo; en haut de la page),</li>
	<li>Cliquer sur le signe &ldquo;+&rdquo; &agrave; gauche du titre de l&rsquo;article</li>
	<li>Cliquer sur le bouton &ldquo;Share&rdquo;</li>
	<li>Dans la fen&ecirc;tre qui appara&icirc;t, renseigner l&rsquo;adresse de la r&eacute;daction (proposition@sens-public.org), puis cliquez sur &ldquo;add&rdquo;, et enfin &ldquo;Share&rdquo;.</li>
	<li>Un courriel doit ensuite &ecirc;tre envoy&eacute; &agrave; l&#39;adresse proposition@sens-public.org pour signaler la soumission.</li>
</ul>

<p>&nbsp;</p>

<h3>Protocole de soumission de dossier</h3>

<p>&nbsp;</p>

<p>Pour proposer un dossier &agrave; la revue, veuillez suivre les instructions relatives ci-dessus pour soumettre une pr&eacute;sentation qui doit contenir :</p>

<ul>
	<li>un titre de dossier (provisoire)</li>
	<li>une courte description argument&eacute;e (200 mots)</li>
	<li>le&middot;s nom&middot;s des directeur&middot;rice&middot;s</li>
	<li>la liste pr&eacute;liminaire des articles composant le dossier ainsi que leurs auteur&middot;e&middot;s</li>
	<li>une courte bibliographie &eacute;dit&eacute;e dans le volet Bibliographie</li>
</ul>

<p>Veuillez ensuite partager la pr&eacute;sentation du dossier avec la r&eacute;daction (proposition@sens-public.org).</p>