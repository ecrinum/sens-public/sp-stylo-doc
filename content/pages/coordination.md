Page cachée pour la coordination de Sens public.

**Étape pour xml érudit :**

- Dans le repos du Gitlab
- Allez dans le répertoire idsp21.csv et compléter les information de l'article
- Mettre varia si l'article n'est pas dans un dossier et mettre l'orseq global de tous les articles.
- Si l'article est dans un dossier, mettre le id du dossier et l'orseq de l'article du dossier (pas le global). Lire le sommaire pour savoir l'ordre des articles ou demander au directeur.trice.
- Info à indiquer : type, id, ordseq
    - ESSAI, 58, 1-99
    - CHRONIQUE, 114, 100-199
    - ENTRETIEN, 113, 200-299
    - LECTURE, 76, 300-399
    - CREATION, 60, 400-499
- Mettre à jour le sommaire xml. (ajouter cette section au note)

1. **Créer le PDF de l'article - sens-public**

- À partir de stylo, créer une version majeure. Cliquer sur cette version et prendre la clé de version. 
- Copier la clé de version (se trouve à la fin de hyperlien de la version de l'article) dans [stylo-export](http://stylo-export.ecrituresnumeriques.ca/) et indiquer le ID de l'artcile (ex. SP1609)
- Choisir comme processor : xelatex
- Sélectionner table des matières seulement s'il y en a une.
- Appuyer sur submit
- Contrôler le PDF : page titre, table des matière, images, bibliographie, etc.

2. **Mettre l'article sur le repo**

- Si PDF est validé, télécharger le fichier zip de [stylo-export](http://stylo-export.ecrituresnumeriques.ca/).
- Valider que les images de l'article sont toutes dans le dossier média du zip. Toutes les images doivent être en png. Nommer les images toujours par le ID de l'article et le numéro. ex. SP1402-img1.png
- Mettre l'image logo et la nommer : artonid.png ou jpg. L'image logo peut être en png ou jpg. Exemple : arton1487.png attention, on ne met pas le SP devant le id et on doit toujours mettre l'extension en minuscule.
- À partir du terminal, sauvegarder dans le [repo](https://framagit.org/laconis/SP-articles) en local dans le répertoire de l'article (nommé ID de l'article).
- Différentes commandes terminal :
    - cd edition2021
    - git pull
    - git add (mettre le numéro de l'article sx. SP1523)
    - git commit -m ""
    - git push 

3. **Publier l'article sur le site de sens-public**

- À partir du terminal, se connecter en ssh à ecrituresnumeriques et entrer les commandes suivantes :
- cd /docker/sphub/articles
- bash update_articles.sh .
- cd docker/sphub/src/docker/
- docker-compose -f docker-compose.prod.yml exec app python manage.py importhtml id_sans_sp

Cette action actualisera les articles déjà publiés, ne fait pas une importation des nouveaux dans le repos.
- Une fois la mise à jour effectuée, on doit absolument publier le sommaire en premier. Toujours à partir du terminal entrer la commande : .....

- À partir de l'interface graphique du site (Django), valider que le sommaire est bien en place et le publier.
- Valider que le sommaire est bien publié et conforme sur le site web Sens public avant de passer à l'étape suivante. 
- À partir du terminal, importer un par un les articles, dans le bon ordre, au dossier en entrant la commande :....
- Retourner sur l'interface graphique et valider que tous les articles sont présents et les publiés. 
- Les mettre dans le bon ordre à partir de l'interface graphique
- Faire un contrôle de qualité sur le site web de Sens public (dossier présent, tous les articles sont présents, images logo, images, liens, etc.)
- À partir du terminal refaire une mise à jour du repos par précaution. Entrer dans la commande : .... 
- À partir de l'interface graphique (Django) aller mettre le dossier en page de garde et valider que le tout est conforme sur le site.
- Finalement, informer les directeurs (dans le cas d'un dossier) ou auteur de la publication.


## Post-publication ##
- La coordonnatrice informe l'auteur.e de la publication
- Dans Stylo, renommer l'article en "PSP*identifiant* et partager l'article au compte ArchiveSP.