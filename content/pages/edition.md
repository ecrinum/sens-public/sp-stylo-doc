
## Présentation *Sens public*
- Tout d'abord, nous vous invitons à prendre connaissance du site de la revue [*sens public*](https://sens-public.org/presentation/).
Vous y trouverez la présentation de la revue, l'équipe, le protocole de publication et les dossiers et articles déjà publiés. 

## Présentation et fonctionnement des différents outils nécessaires
Plusieurs outils seront nécessaires pour éditer les articles de *sens public*. Certains demandent la création d'un compte. Il est important de bien prendre connaissance des différents outils et de lire leur documentation. 

- [**Stylo**](https://stylo.huma-num.fr/) : La chaîne éditoriale de la revue *sens public* est basée sur l'éditeur de texte sémantique *Stylo*. Il s'agit donc d'un outil essentiel à votre travail. Tout d'abord, vous devez vous créer un compte stylo et lire [la documentation](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md). Il est impératif de bien maîtriser l'outil *Stylo* pour éditer les articles. Notez qu'une [permanence](https://meet.jit.si/stylo) tous les jeudis de 11h00 à 12h00 est assurée pour vous aider lors de l'édition d'un texte. 

- **Markdown** : L'éditeur de texte *Stylo* utilise le langage de balisage Markdown. Vous trouverez les fonctions de base [ici](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!pages/syntaxemarkdown.md).

- [**Hypothes.is**](https://web.hypothes.is/) : Outil d'annotation. L'outil hypothes.is sera utilisé pour annoter les articles. Vous devez donc vous créer un compte. Les commentaires et/ou questions portant sur les articles seront indiqués via hypothes.is. Il est important de noter qu'il y a deux possibilités d'annotation soit annoter une version ou l'article. Pour plus de détails, voir la documentation de stylo.

- [**Orcid**](https://orcid.org/) : Le site orcid est utilisé afin de trouver les orcid des auteur.es. Il n'est pas nécessaire pour cette tâche de se créer un compte.

- [**Zotero**](https://www.zotero.org/) : Logiciel de gestion de références nécessaire pour la validation de la bibliographie. Pour l'installation et la documentation voir le [site offert par l'UDM](https://bib.umontreal.ca/citer/logiciels-bibliographiques/zotero/installer).

- [**Zbib.org**](https://zbib.org/) : Cet outil permet de trouver rapidement des référence bibliographique.

- **bibtex**: Sur Zotero, on exporte une collection ou une référence en particulier vers `.bib` *aka BibTex* : format de bibliographie associé à LaTeX. C'est un fichier texte brut. C'est ce format que l'on importe sur Stylo, pour chaque article. La clé BibTex figure sur un fichier `.bib` après l'info sur le type de publication (précédée par un `@`), entre accolades.

- [**Imgur**](https://imgur.com/) : Outil pour l'hébergement d'images des articles provenant de *Stylo*. La création d'un compte est nécessaire.

- [**Framateam**](https://framateam.org/) : Outil de communication avec la coordonatrice de *sens public* et de l'équipe. La création d'un compte est nécessaire.

- [**Meistertask**](https://www.meistertask.com/) : Outil de gestion des tâches et calendrier. La création d'un compte est nécessaire.


## Édition d'un article dans *Stylo* 

La coordonnatrice attribue les articles aux éditeurs.

1.  Identifiant pour l'article 
* Tous les articles de la revue *sens public* ont un ID propre (4 chiffres, ex. 1502). Le ID est attribué par la coordonnatrice. En aucun cas, le ID de l'article doit être modifié. 
Selon où l'article est rendu dans la chaîne d'édition, il y aura des lettres devant l'ID: 
Voici le protocole de nommage de l'article :

  - **ED**XXXX (article en édition)
  - **CQ**XXXX (article prêt pour le contrôle qualité et en cours de contrôle qualité)
  - **AU**XXXX (article prêt à être envoyé et envoyé à l'auteur dans sa version html)
  - **SP**XXXX (article prêt à publié)
  - **PSP** XXXX (article publié et pushé sur git par l'éditeur en chef)

2. Prendre connaissance rapidement de l'article
*  Faire un rapide survol de l'article et des annotations hypothes.is de l'auteur. Si on voit des problèmes majeurs, informer immédiatement la coordonnatrice.

3. Nettoyage de l'article :
*  s'assurer que l'article est bien identifié par son identifiant
*  valider les niveaux des titres
*  Valider les espaces insécables
*  créer une version majeure de l'article "Espaces insécables"

4. Métadonnées de l'articles :
*  entrer les métadonnées de l'article dans Stylo : auteur (nom, prénom et orcid), si l'orcid de l'auteur n'est pas indiqué, le chercher sur [Orcid](https://orcid.org/). Si non présent, annoté pour demande à l'auteur.
*  valider la présence des mots-clefs choisis par l'auteur, 
*  valider la présence des résumés (en français et anglais, ou, en langue de l'article et français), 
*  valider la présence URL de l'article *Sens public*, 
*  valider la présence date de publication (elle sera définie par la coordonnatrice lors de la publication), identifiant, 
*  valider la présence titre (et sous-titre le cas échéant)
*  valider la présence langue de l'article
*  valider la présence Bibliographie : choix *All citations*. 
À noter que les autres éléments, tels que les mots-clefs contrôlés seront validés à l'étape ultérieure, CQ de l'article. Voir section CQ. 
*  créer une version majeure "Renseignement des métadonnées"

5. Édition de l'article :
* Si ce n'est pas fait, entrer le bibtex dans le module "Bibliography"
* créer une version majeure "Importation bibliographie"
* Prendre connaissance des annotations hypothes.is et faire les demandes de modification. Valider que les modifications ont été faites dans hypothes.is en indiquant Fait. Si nécessaire, mettre des notes à l'auteur si besoin d'informations supplémentaires.
* créer une version majeure "modifications auteur"
* insérer dans le texte ou valider les clés bibtex
* structurer ou valider les citations longues (sans guillemets)
* nettoyage des doubles espaces 
* structurer ou valider les images de l'article : Toutes les images de l'article doivent être en format png et non jpg. Donc convertir le format si nécessaire ([site de conversion](https://png2jpg.com/fr/) disponible). Déposer les images png dans [Imgur](https://imgur.com/) et prendre le lien de imgur et l'indiquer dans l'article au bon emplacement en markdown. **Simplement changer l'extension de l'image manuellement n'est pas suffisant**.
* créer une version majeure "Édition de l'article"

Informer la coordonnatrice que l'édition de l'article est terminée.

Envoi de l'article à l'auteur.e et retour.

6. Correction nécessaire ou ajout d'informations manquantes
*  intégrer les éventuelles modifications demandées par l'auteur.e, renseignées en annotation hypothesis dans le groupe *spAuteur* dans la preview de l'article et ajouter les informations demandées qui étaient manquantes. 
* renommer l'article en "CQ*identifiant*" 
* créer une version majeure "Prêt pour CQ"
* informer la Coordination SP que l'article est prêt pour CQ

## CQ - BAT

Plusieurs éléments sont à valider à l'étapes de la CQ. 

* Valider rapidement l'édition de l'article (niveau de titre, bibliographie, note, etc)

Dans les métadonnées valider :

* renseigner la date de publication dans les métadonnées Stylo
* renseigner l'id **avec** SP (majuscule) (ex SP1566)
* Vérifier les mots-clés auteur et éditeur en les confrontant avec les mots-clés existants sur le site sens-public.org. Éventuellement éditer le raw
* Vérifier que l'url de l'article soit renseigné en entier (ex: /articles/1509)
* S'il y a une légende pour le logo, ajouter à la fin du yaml raw: `logocredits: "Texte de la légende"`
* Vérifier l'orthographe de la revue dans les métadonnées : Sens public
* Vérifier que le directeur de la revue soit le bon : Gérard Wormser 0000-0002-6651-1650
* Vérifier qu'il y ait un espace avant chaque mot-clé auteur à partir du deuxième (et ensuite vérifier dans le pdf)
* Vérifier ISSN 2104-3272
* Vérifier que, le cas échéant, les informations de traductions soient renseignées
* Vérifier que, le cas échéant, les informations sur le dossier soient bien renseignées (titre exact) et identifiant "SP+id" et le directeur (nom et orcid). Si plusieurs les mettre toujours dans le même ordre.
* créer une version majeure "Version export 1"
* renommer l'article "SP*identifiant*"
* copier la clef de version à partir de l'URL de la version "Version export 1"
* coller la clef de version dans l'onglet *Version* du [Stylo export for Sens public](https://stylo-export.ecrituresnumeriques.ca/exportsenspublic.html)
* renseigner l'*Identifiant* avec "SP*identifiant*"
* vérfier que le processor est bien renseigné en mode "xelatex"
* exporter
* contrôler les erreurs affichées en rouge
* contrôler le PDF généré en ligne (auteur, métadonnées, date, notes, bibliographie)
* télécharger le dossier .zip
* ajouter le dossier SP+id dans SP-articles/année_de_publication/
* ajouter dans le dossier media le logo article avec la nomenclature `arton+id_sans_sp.ext` par exemple `arton1509.jpg`

### Mots-clés

Prendre note que les mots-clés contrôlés sont choisis par la coordonnatrice de la revue, ce n'est pas à l'éditeur les trouver.

Dans stylo, il y a deux types de mots-clés:
- Mots-clés auteurs : Mots-clés choisis par l'auteur, on demande de se limiter à 6 mots clés
- Mots-clés contrôlés : Mots-clés déjà existants sur le site de sens public

Important, lors de la CQ, on doit valider que les mots-clés existent bel et bien sur le site de sens public.

- À partir de l'outil stylo, dans les métadonnées, dans l'onglet yaml, trouver les mots-clés contrôlés. Voici un exemple :
controlledKeywords:
  - idRameau: FRBNF120218114
    label: Arts et Lettres
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb12021811z
  - idRameau: FRBNF133328055
    label: Monde numérique
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb133328054
  - idRameau: FRBNF13318593
    label: Édition, presse et médias
    uriRameau: http://catalogue.bnf.fr/ark:/12148/cb13318593f

    Les majuscules au début de l'idRameau sont nécessaires.

- À partir du site de sens public, sélectionner Index (en haut à gauche) et par la suite mots-clés.
- Trouver le premier mot-clé contrôlé du yaml dans la liste.
- Valider qu'il est bien présent et qu'il s'agit bel et bien d'un mot-clé contrôlé soit nommé Mot-clés — FR Éditeur
- En sélectionnant rameau, valider que le ID et le URI sont les mêmes que dans le yaml.
- Refaire les mêmes étapes pour chacun des mots-clés contrôlés du yaml.
 
### Valider le PDF de l'export *Stylo Sens public*

* À partir de stylo, créer une version majeure nommée version export. Cliquer sur cette version et prendre la clé de version. 
* Copier la clé de version (se trouve à la fin de hyperlien de la version de l'article) dans [stylo-export](http://stylo-export.ecrituresnumeriques.ca/) et indiquer le ID de l'article (ex. SP1609)
* Choisir comme processor : xelatex
* Sélectionner table des matières seulement s'il y en a une.
* Appuyer sur submit
* Contrôler le PDF : page titre, table des matière, images, bibliographie, etc.
* Faire une version majeure nommée : CQ terminé

Informer la coordonnatrice que le contrôle de Qualité est terminé.

// Une version majeure "Version pour auteur.e" sera créée par la Coordination SP qui informera l'auteur.e pour sa révision de l'édition de l'article. //

// La Coordination se charge d'obtenir le Bon À Tirer (BAT) de la part de l'auteur.e pour confirmer à l'éditeur l'export de l'article. //

## Retour de l'auteur.e et si modifications, les finaliser.