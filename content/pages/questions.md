

### Pourquoi mon image ne fonctionne-t-elle pas lors de l'export ? 

Si une image n'apparaît pas dans pdf d'export mais apparaît dans la preview de l'article, il peut y a voir plusieurs raisons en cause : 

- l'image est dans un format qui n'est pas géré lors de l'export : les images JPG ne sont pas prises en compte. 

--> Solution : Il faut alors convertir l'image au format PNG. 

- l'image est de taille trop grande : 

--> Solution : 

- la légende de l'image est vide dans le markdown de l'article

### Pourquoi ma bibliographie n'est pas acceptée par l'outil bibliographique ? 

### Pourquoi mes liens HTTP dépassent de la mise en page lors de l'export PDF ? 

