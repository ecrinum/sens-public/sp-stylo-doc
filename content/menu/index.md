---
headless: true
---

- **[Auteur·e·s]({{< relref "/auteurs" >}})**
- **[Éditeur·rice·s]({{< relref "/edition" >}})**
- **[Relecteur·rice·s]({{< relref "/relecteurs" >}})**
- **[Questions fréquentes]({{< relref "/questions" >}})**


---

**Liens**

- [Documentation générale de Stylo](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md)
- [Site de Sens public](http://sens-public.org/)
- [Permanence Stylo (jeudi 11h-12h)](https://meet.jit.si/stylo)