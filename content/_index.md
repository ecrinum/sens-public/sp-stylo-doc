---
title: Documentation Stylo pour Sens public
---

Bienvenue dans la documentation pour les utilisations de l'éditeur de texte pour SHS [Stylo](https://stylo.huma-num.fr/) dans le cadre de la revue [Sens public](http://sens-public.org/).

Ce site rassemble les informations pour : 

- les [auteur·e·s de Sens public](/auteurs) 
- les [éditeur·rice·s de Sens public](/edition)
- les [relecteur·rice·s de Sens public](/relecteurs)

Une documentation pour répondre aux questions fréquemment posées est également disponible. 


<div class="logo">
    <img src="images/ecritures-numériques-logo.svg">
    <img src="images/sp_logo_block.svg">
    <img src="images/logo_stylo.svg">
</div>